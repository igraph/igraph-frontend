import VueI18n from "vue-i18n";

export class TranslationService {
  static availableLanguages = ["en", "ru"];
  private i18n: VueI18n | undefined = undefined;
  initLocale($i18n: VueI18n) {
    this.i18n = $i18n;
    const userLanguage = this.getUserLanguage();
    if (
      userLanguage &&
      TranslationService.availableLanguages.includes(userLanguage)
    ) {
      this.setLocale(userLanguage);
    }
  }
  get currentLocale() {
    return this.i18n?.locale;
  }
  getUserLanguage(): string | undefined {
    const userLanguage = window.navigator.language;
    return userLanguage ? userLanguage.substr(0, 2).toLowerCase() : undefined;
  }
  setLocale(newLocale: string) {
    if (!this.i18n) {
      return;
    }
    this.i18n.locale = newLocale;
    const htmlElement = document.documentElement;
    htmlElement.setAttribute("lang", newLocale);
  }
}

export const translationService = new TranslationService();
