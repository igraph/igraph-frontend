import Vue from "vue";
import App from "./App.vue";
// import "./registerServiceWorker";
import router from "./router";
// import store from "./store";
import i18n from "./config/i18n";
import {
  BButton,
  BNavbar,
  BNavbarBrand,
  BNavbarNav,
  BNavbarToggle,
  BNavItem,
  BCollapse,
  BFormSelect,
} from "bootstrap-vue";
import { translationService } from "@/services/translation";

Vue.config.productionTip = false;
Vue.component("BButton", BButton);
Vue.component("BNavbar", BNavbar);
Vue.component("BNavbarBrand", BNavbarBrand);
Vue.component("BNavbarNav", BNavbarNav);
Vue.component("BNavbarToggle", BNavbarToggle);
Vue.component("BNavItem", BNavItem);
Vue.component("BCollapse", BCollapse);
Vue.component("BFormSelect", BFormSelect);

translationService.initLocale(i18n);

new Vue({
  router,
  // store,
  i18n,
  render: (h) => h(App),
}).$mount("#app");
